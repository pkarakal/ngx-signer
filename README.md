## @universis/ngx-signer

Universis Signer client modules for angular

### Installation

    npm i @universis/ngx-signer

### Usage 

Import `NgxSignerModule` in app modules

    ...
    import { NgxSignerModule } from '@universis/signer';

    @NgModule({
    declarations: [
        ...
    ],
    imports: [
        ...
        NgxSignerModule.forRoot()
    ],
        providers: [
            ...
        ],
        bootstrap: [ AppComponent ]
    })
    export class AppModule {

    }

### Development

This project is intended to be used as a part of an angular cli project. 

- Add this project as git submodule

        git submodule add https://gitlab.com/universis/ngx-signer.git packages/ngx-signer

- Edit angular.json and add `ngx-signer` configuration under `projects` node:

        "ngx-signer": {
            "root": "packages/ngx-signer",
            "sourceRoot": "packages/ngx-signer/src",
            "projectType": "library",
            "prefix": "universis",
            "architect": {
                "build": {
                    "builder": "@angular-devkit/build-ng-packagr:build",
                    "options": {
                        "tsConfig": "packages/ngx-signer/tsconfig.lib.json",
                        "project": "packages/ngx-signer/ng-package.json"
                    }
                },
                "test": {
                "builder": "@angular-devkit/build-angular:karma",
                "options": {
                    "main": "packages/ngx-signer/src/test.ts",
                    "tsConfig": "packages/ngx-signer/tsconfig.spec.json",
                    "karmaConfig": "packages/ngx-signer/karma.conf.js"
                }
                },
                "lint": {
                "builder": "@angular-devkit/build-angular:tslint",
                "options": {
                    "tsConfig": [
                        "packages/ngx-signer/tsconfig.lib.json",
                        "packages/ngx-signer/tsconfig.spec.json"
                    ],
                    "exclude": [
                    "**/node_modules/**"
                    ]
                }
            }
        }
        }

- Import `@universis/ngx-signer` in tsconfig.json#paths

        {
            ...
            "compilerOptions": {
                ...
                "paths": {
                    ...
                    "@universis/ngx-signer": [
                        "packages/ngx-signer/src/public_api"
                    ]
                }
            }
        }