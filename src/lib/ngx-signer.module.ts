import { ModuleWithProviders, NgModule } from '@angular/core';
import { SharedModule } from '@universis/common';
import { AdvancedFormsModule } from '@universis/forms';
import { ModalModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { VerificationCodeFormComponent } from './components/verification-code-form/verification-code-form.component';
import { SignerService } from './signer.service';
import { DocumentSignComponent } from './components/document-sign/document-sign.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { FormioModule } from 'angular-formio';
import { DocumentSignActionComponent } from './components/document-sign-action/document-sign-action.component';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ModalModule,
    FormioModule,
    AdvancedFormsModule,
    TranslateModule,
    NgxDropzoneModule
  ],
  declarations: [
    VerificationCodeFormComponent,
    DocumentSignComponent,
    DocumentSignActionComponent
  ],
  exports: [
    VerificationCodeFormComponent,
    DocumentSignComponent,
    DocumentSignActionComponent
  ],
  entryComponents: [
    VerificationCodeFormComponent,
    DocumentSignComponent,
    DocumentSignActionComponent
  ]
})
export class NgxSignerModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NgxSignerModule,
      providers: [
        SignerService
      ]
    };
  }
}
