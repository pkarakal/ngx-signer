import { TestBed } from '@angular/core/testing';

import { SignerService } from './signer.service';

describe('NgxSignerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SignerService = TestBed.get(SignerService);
    expect(service).toBeTruthy();
  });
});
